import Config from '@/assets/config.json'
import Vue from 'vue'
import VueResource from 'vue-resource'
import Vuex from 'vuex'

import App from './App.vue'

Vue.config.productionTip = false
Vue.use(Vuex)
Vue.use(VueResource)

var store = new Vuex.Store({
  state: {posts: [], config: Config.config},
  mutations: {
    setConfig: function(state, config) {
      state.config = config
    },
    setPosts: function(state, posts) {
      state.posts = posts
    }
  }
});

new Vue({
  render: h => h(App),
  store,
  http: {root: Config.config.serverUrl},
}).$mount('#app')
